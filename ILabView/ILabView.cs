﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface ILabView
{
    //Test Input will output the length of the dimensions of Input
    string TestInput(double Input);
    string TestInput(double[] Input);
    string TestInput(double[,] Input);
    string TestInput(double[,,] Input);

    //Banner Check Crops Iin and compares it to Itarget. Output is max difference.
    //double LVBannerCheck(double[,,] Iin, double[,,] Itarget);
    //Shows the error for BannerCheck
    //double LVBannerCheck(out double[,,] Iout, double[,,] Iin, double[,,] Itarget);
    double LVBannerCheck(double[,,] Iin, double[,,] Itarget);
    double LVBannerCheck(out double[,,] Iout, double[,,] Iin, double[,,] Itarget);

    //Creates a Banner Target from an image
    void MakeBannerTarget();
    void MakeBannerTarget(double[,,] Iin);

    //Video Level outputs the mean, standard deviation and rgb means for Iin
    double[] LVVideoLevel(double[,,] Iin);

    //Color Chart Locates 6 of the 24 color on DSC Chart and outputs rgb means for each.
    double[,] LVColorChart(double[,,] Iin);
    //Use this version to get Iout which shows the selection boxes used to find M.
    void LVColorChart(out double[,] M, out double[,,] Iout, double[,,] Iin);
}