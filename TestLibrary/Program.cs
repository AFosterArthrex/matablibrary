﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace TestMatlabFunctions
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Loading Library");
            ILabView B = new LabViewLibrary.LVClassILabView();
            Console.WriteLine("Library Loaded");
            Console.WriteLine("");

            Console.Write("Loading Images");
            Image InputImage = Image.FromFile("InputImage.bmp");
            Console.Write(".");
            double[,,] dInputImage = ImageToDoubleArray(InputImage);
            Console.Write(".");

            Image BannerTarget = Image.FromFile("BannerTarget.bmp");
            Console.Write(".");
            double[,,] dBannerTarget = ImageToDoubleArray(BannerTarget);
            Console.Write(".");

            Console.Write(".");
            Image InputImage2 = Image.FromFile("ColorChart1.bmp");
            Console.Write(".");
            double[,,] dInputImage2 = ImageToDoubleArray(InputImage2);
            Console.Write(".");

            Console.Write("Done!");
            Console.WriteLine("");

            Console.WriteLine("Testing TestInput");
            string Output = B.TestInput(dInputImage);
            Console.WriteLine(Output);
            Console.WriteLine("");

            Console.WriteLine("Testing LVBannerCheck");

            //double Error = B.BannerError(dInputImage, dBannerTarget);
            double[,,] dBannerError = new double[3, 1080, 760];
            double Error = B.LVBannerCheck(out dBannerError, dInputImage, dBannerTarget);
            Console.WriteLine("Banner Error:");
            Console.WriteLine(Error);


            //double[,,] dBannerError = new double[3,1080,760];
            //B.LVBannerCheck(out dBannerError, out Error, dInputImage, dBannerTarget);
            //double[,,] dBannerError = B.BannerError(out Error, dInputImage, dBannerTarget);
            Console.WriteLine("Saving BannerError.bmp");
            Image BannerError = DoubleArrayToImage(dBannerError);

            BannerError.Save("BannerError.bmp");
            Console.WriteLine("");

            Console.WriteLine("Testing LVColorChart");

            double[,] Means = new double[6, 3];

            //Test Overloaded LVColorChart
            B.LVColorChart(out Means, out dInputImage, dInputImage2);

            Means = B.LVColorChart(dInputImage2);

            Console.WriteLine("ColorChart Output:");

            string MeansString = "";

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    MeansString += Means[i, j].ToString();
                    if (j < 2)
                    {
                        MeansString += ", ";
                    }
                }
                if (i < 5)
                {
                    MeansString += System.Environment.NewLine;
                }
            }
            Console.WriteLine(MeansString);

            Console.WriteLine("Saving OutputImage.bmp");
            Image OutputImage = DoubleArrayToImage(dInputImage);

            OutputImage.Save("OutputImage.bmp");

            Console.WriteLine("");
            Console.WriteLine("Testing LVVideoLevel");

            double[] videostats = B.LVVideoLevel(dInputImage2);
            string VideoString = "";
            for (int j = 0; j < 5; j++)
            {
                VideoString += videostats[j].ToString();
                if (j < 4)
                {
                    VideoString += ", ";
                }
            }
            Console.WriteLine("Video Level Output:");

            Console.WriteLine(VideoString);

            Console.WriteLine("");
            Console.WriteLine("TestLibrary Complete!");
        }

        static double[,,] ImageToDoubleArray(Image image)
            //Outputs a double array of [3, height, width]
        {
            int width = image.Width;
            int height = image.Height;

            Bitmap imageB = (Bitmap)image;

            double[,,] imageD = new double[3, height, width];

            int i, j;
            for (i = 0; i < height; i++)
            {
                for (j = 0; j < width; j++)
                {
                    Color pixelColor = imageB.GetPixel(j, i);
                    imageD.SetValue(pixelColor.R, 0, i, j);
                    imageD.SetValue(pixelColor.G, 1, i, j);
                    imageD.SetValue(pixelColor.B, 2, i, j);
                }
            }

            return imageD;
        }

        static Image DoubleArrayToImage(double[,,] imageD)
            //Assumes a double array of [3, height, width]
        {
            int height = imageD.GetLength((int)1);
            int width = imageD.GetLength((int)2);

            Bitmap imageB = new Bitmap(width, height);

            int i, j;
            for (i = 0; i < height; i++)
            {
                for (j = 0; j < width; j++)
                {
                    Color pixelColor = Color.FromArgb((int)imageD[0, i, j], (int)imageD[1, i, j], (int)imageD[2, i, j]);
                    imageB.SetPixel(j, i, pixelColor);
                }
            }
            Image outputimage = (Image)imageB;

            return outputimage;
        }
    }
}
